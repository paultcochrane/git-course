# Git Course

A course in learning how to use the version control system [Git](http://git-scm.com/).

## Building the slides from source

First, clone the project repository and enter the project directory:

```
git clone git@gitlub.com:paultcochrane/git-course.git
cd git-course
```

Install the required dependencies.  On a Debian-based system this looks
like:

```
sudo apt-get install -y texlive-latex-extra latex-beamer texlive-lang-german \
    texlive-fonts-extra texlive-fonts-recommended texlive-xetex lmodern make
```

Then enter the `git` directory and run `make` to build the project:

```
cd git
make
```
